# homepage

Forked from GitHub from ![this](https://github.com/nathanielevan/homepage "homepage") page.  

Custom browser homepage made by Nathaniel Evan. Inspired by various sources. Made with vanilla HTML-CSS-JS and designed with responsiveness in mind, based on Gruvbox Material colours by [@sainnhe](https://github.com/sainnhe). As seen in [this Reddit post](https://www.reddit.com/r/startpages/comments/wagyek/my_first_custom_startpage_based_on_gruvbox/) (for verification purposes).

Feel free to fork and customise to your needs, or help me add new features or fix my incomprehensible code by submitting a pull request!

